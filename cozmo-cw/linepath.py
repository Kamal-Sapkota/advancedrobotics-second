#!/usr/bin/env python3

# Copyright (c) 2016 Anki, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License in the file LICENSE.txt or at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

'''Drive And Turn

Make Cozmo drive forwards and then turn 90 degrees to the left.
'''


#there are 8 movements in total including therrotation and going in a straight line

import cozmo
import time
from cozmo.util import degrees, distance_mm, speed_mmps

import numpy as np

interval = 0.05

def cozmo_program(robot: cozmo.robot.Robot):
    movement = 0
    trackspeed = []
    distance = robot.drive_straight(distance_mm(500), speed_mmps(50))
    while(distance.is_running): #tracking the movement
        lspeed = robot.left_wheel_speed.speed_mmps
        rspeed = robot.right_wheel_speed.speed_mmps

        trackspeed.append([lspeed,rspeed])#adding them movemnt

        time.sleep(interval)
    np.save('linepath1',trackspeed)



cozmo.run_program(cozmo_program)
