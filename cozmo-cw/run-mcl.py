#!/usr/bin/env python3##


import asyncio

import cozmo

from frame2d import Frame2D 
from map import CozmoMap, plotMap, loadU08520Map, Coord2D
from matplotlib import pyplot as plt
from cozmo_interface import cube_sensor_model
from cozmo_interface import velocity_to_track_speed, target_pose_to_velocity_spline, track_speed_to_pose_change,cozmoOdomNoiseX,cozmoOdomNoiseY,cozmoOdomNoiseTheta
from mcl_tools import *
from gaussian import Gaussian, GaussianTable, plotGaussian
from cozmo.util import degrees, distance_mm, speed_mmps
import math
import numpy as np
import threading
import time

	

# this data structure represents the map
m=loadU08520Map()

# this probability distribution represents a uniform distribution over the entire map in any orientation
mapPrior = Uniform(
		np.array([m.grid.minX(),m.grid.minY(),0]),
		np.array([m.grid.maxX(),m.grid.maxY(),2*math.pi]))

HANDOUT = False

# TODO Major parameter to choose: number of particles
numParticles = 100

# The main data structure: array for particles, each represnted as Frame2D
particles = sampleFromPrior(mapPrior,numParticles)

#noise injected in re-sampling process to avoid multiple exact duplications of a particle
# TODO Choose sensible re-sampling variation
xyaResampleVar = np.diag([20,20,3*math.pi/180])
# note here: instead of creating new gaussian random numbers every time, which is /very/ expensive,
# 	precompute a large table of them an recycle. GaussianTable does that internally
xyaResampleNoise = GaussianTable(np.zeros([3]),xyaResampleVar, 200)

# Motor error model
xyaNoiseVar = np.diag([cozmoOdomNoiseX,cozmoOdomNoiseY,cozmoOdomNoiseTheta])
xyaNoise = GaussianTable(np.zeros([3]),xyaNoiseVar,200)


posiblePosition = Frame2D()## knowing the possible position using the frame2D


def runMCLLoop(robot: cozmo.robot.Robot):
	global particles
	global posiblePos ## this is making possiblePos as global 

	
	
	particleWeights = np.zeros([numParticles])
	cubeIDs = [cozmo.objects.LightCube1Id,cozmo.objects.LightCube2Id,cozmo.objects.LightCube3Id]

	# main loop
	timeInterval = 0.1
	t = 0
	while True:
		t0 = time.time()
		cubeSeen = False
		
		# read cube sensors
		robotPose = Frame2D.fromPose(robot.pose)
		cubeVisibility = {}
		cubeRelativeFrames = {}
		numVisibleCubes = 0
		for cubeID in cubeIDs:
			cube = robot.world.get_light_cube(cubeID)
			relativePose = Frame2D()
			visible = False
			if cube is not None and cube.is_visible:
				cubePose = Frame2D.fromPose(cube.pose)
				relativePose = robotPose.inverse().mult(cubePose)
				visible = True
				cubeSeen = True ## cube seen is made true
				numVisibleCubes = numVisibleCubes+1
			cubeVisibility[cubeID] =  visible
			cubeRelativeFrames[cubeID] =  relativePose

		# read cliff sensor
		cliffDetected = robot.is_cliff_detected

		# read track speeds
		lspeed = robot.left_wheel_speed.speed_mmps
		rspeed = robot.right_wheel_speed.speed_mmps

		# read global variable
		currentParticles = particles

		# MCL step 1: prediction (shift particle through motion model)
		# For each particle
		#print(len(currentParticles))
		for i in range(0,numParticles):
			
			# TODO this is all wrong...
			
			# TODO Instead: shift particles along deterministic motion model, then add perturbation with xyaNoise (see above)
			currentParticles[i] = currentParticles[i].mult(track_speed_to_pose_change(lspeed, rspeed, timeInterval))##mult multiply the matrices
			#noize = xyaNoise.sample()
			#currentParticles[i] = Frame2D.fromXYA(currentParticles[i].x() + noize[0], currentParticles[i].y() + noize[1], currentParticles[i].angle() + noize[2])
			
			#currentParticles[i] = currentParticles[i].mult(Frame2D.fromXYA(0.1 * dy, -0.1 * dx, 0))
		# See run-odom-vis.py under "Run simulations" 

		# MCL step 2: weighting (weigh particles with sensor model)
		weighing = 0 ##bigger weight
		index = 0     ##index
		for i in range(0,numParticles):
			# TODO this is all wrong (again) ...
			# TODO instead, assign the product of all individual sensor models as weight (including cozmo_cliff_sensor_model!)
			pot = cozmo_cliff_sensor_model(currentParticles[i], m, cliffDetected)    ##
			invertingFrame = Frame2D.fromXYA(currentParticles[i].x(),currentParticles[i].y(),currentParticles[i].angle()).inverse() ## takes the current position of the particles in the x, y and theta position
			for cubeID in cubeIDs:##calculating the probability of robot being in aprticle
				relativeTruePose = invFrame.mult(m.landmarks[cubeID])##
				pot = pot * cube_sensor_model(relativeTruePose,cubeVisibility[cubeID],cubeRelativeFrames[cubeID])##
			
			particleWeights[i] = pot##
			if(weighing < particleWeights[i]):##the particle with biggtst weight
				index = i##
				biggerWeight = particleWeights[i]##
		posiblePosition = currentParticles[index]    ## give the best positon , how possible of the robot in that particle of that robot.
		# See run-sensor-model.py under "compute position beliefs"

		# MCL step 3: resampling (proportional to weights)
		# TODO not completely wrong, but not yet solving the problem
		

		# TODO Draw a number of "fresh" samples from all over the map and add them in order 
		# 		to recover form mistakes (use sampleFromPrior from mcl_tools.py)

		#fresh = sampleFromPrior(mapPrior, 5)
		number_of_fresh = 9 if cubeSeen else 0    # 10 or 0 prticles
		fresh = sampleFromPrior(mapPrior,number_of_fresh)  #will create 10 or zero dependingo 

		# TODO Keep the overall number of samples at numParticles

		#numParticles = len(newParticles)

		# TODO Compare the independent re-sampling with "resampleLowVar" from mcl_tools.py
		# TODO Find reasonable amplitues for the resampling noise xyaResampleNoise (see above)
		newParticles = resampleLowVar(currentParticles, particleWeights, numParticles, xyaResampleNoise)
		# TODO Can you dynamically determine a reasonable number of "fresh" samples.
		# 		For instance: under which circumstances could it be better to insert no fresh samples at all?
		
		# write global variable
		
                
                latestParticles = np.append(latestParticles, fresh)##appending it to the list
		particles = latestParticles##
		

		#print("t = "+str(t))
		t = t+1

		t1 = time.time()
		timeTaken = t1-t0
		if timeTaken < timeInterval:
			time.sleep(timeInterval - timeTaken)
		else:
			print("Warning: loop iteration tool more than "+str(timeInterval) + " seconds (t="+str(timeTaken)+")")

def runPlotLoop(robot: cozmo.robot.Robot):
	global particles

	# create plot
	plt.ion()
	plt.show()
	fig = plt.figure(figsize=(8, 8))
	ax = fig.add_subplot(1, 1, 1, aspect=1)

	ax.set_xlim(m.grid.minX(), m.grid.maxX())
	ax.set_ylim(m.grid.minY(), m.grid.maxY())

	plotMap(ax,m)

	particlesXYA = np.zeros([numParticles,3])
	for i in range(0,numParticles):
		particlesXYA[i,:] = particles[i].toXYA()
	particlePlot = plt.scatter(particlesXYA[:,0],particlesXYA[:,1], color="red",zorder=3,s=10, alpha=0.5)

	empiricalG = Gaussian.fromData(particlesXYA[:,0:2])
	gaussianPlot = plotGaussian(empiricalG, color="red")

	# main loop
	t = 0
	while True:
		# update plot	
		for i in range(0,numParticles):
			particlesXYA[i,:] = particles[i].toXYA()
		particlePlot.set_offsets(particlesXYA[:,0:2])

		empiricalG = Gaussian.fromData(particlesXYA[:,0:2])
		plotGaussian(empiricalG, color="red", existingPlot=gaussianPlot)

		plt.scatter(150,200, color="blue",zorder=3,s=10, alpha=0.5)

		plt.draw()
		plt.pause(0.001)
		
		time.sleep(0.01)


def cozmo_program(robot: cozmo.robot.Robot):
	global posiblePos
	timeInterval = 0.1

	threading.Thread(target=runMCLLoop, args=(robot,)).start()
	threading.Thread(target=runPlotLoop, args=(robot,)).start()

	robot.enable_stop_on_cliff(True)

	# main loop
	# TODO insert driving and navigation behavior HERE

	#t = 0
	#targetPose=Frame2D.fromXYA(400,700,0.5*3.1416) # this is the target position
	#relativeTarget = targetPose
	differentTargets = []
	targetPose=Frame2D.fromXYA(180,320,0.5*3.1416)
	differentTarget.append(targetPose)
	targetPose=Frame2D.fromXYA(400,700,0.5*3.1416)
	differentTarget.append(targetPose)
       
        

	#explore, move
	action = 'move1'
	
	timer = 0

	while True:
		

		if(action == "move1"):
			robot.turn_in_place(degrees(360),speed=degrees(20)).wait_for_completed()
			
		
			action = 'move'
		elif(action == "move"):
			if(timer < 100):
				timer += 1
			else:
				timer = 0
				action = 'move1'
		#spline approach
			currentPose = posiblePos
			relativeTarget = currentPose.inverse().mult(targetPose)
			print("relativeTarget"+str(relativeTarget))

			velocity = target_pose_to_velocity_spline(relativeTarget)#target pose to velocity is the method for spline driving
			print("velocity"+str(velocity))

			trackSpeed = velocity_to_track_speed(velocity[0],velocity[1])
			print("trackSpeedCommand"+str(trackSpeed))

			robot.drive_wheel_motors(l_wheel_speed=trackSpeed[0],r_wheel_speed=trackSpeed[1])
			print("currentPose"+str(currentPose))

			print()
		print(action)
		print()
		time.sleep(timeInterval)
		
		


cozmo.robot.Robot.drive_off_charger_on_connect = False
cozmo.run_program(cozmo_program, use_3d_viewer=False, use_viewer=False)



		

