#!/usr/bin/env python3

from frame2d import Frame2D
import math

import numpy as np


#TODO find sensible wheel distance parameter (determine experimentally)
wheelDistance=83



#TODO find sensible noise amplitudes for motor model
cozmoOdomNoiseX = 0.0003
cozmoOdomNoiseY = 0.0003
cozmoOdomNoiseTheta = 0.0000018




# Forward kinematics: compute coordinate frame update as Frame2D from left/right track speed and time of movement
def track_speed_to_pose_change(left, right, time):
    # TODO
	rightDistance = right*time #calculate the right distance
	leftDistance = left*time   #calculate the left distance
	theta = (rightDistance - leftDistance)/wheelDistance   #calculate the theta
	x = (rightDistance+leftDistance) / 2
	y = 0

 
	 #check if this is the rotational or not
	if(abs(theta) > 0.001):
	    rotationalMatrix = (rightDistance + leftDistance) / (2 * theta)
	    x = np.sin(theta) * rotationalMatrix
	    y = (np.cos(theta) - 1) * (-1*rotationalMatrix)
	 
	
	return Frame2D.fromXYA(x, y, theta)
        

# Differential inverse kinematics: compute left/right track speed from desired angular and forward velocity
def velocity_to_track_speed(forward, angular):
    
    # TODO
#implemented the left wheel velocity and right wheel velocity

    leftVelocity = forward - angular * (wheelDistance/2)  # calculate the left wheel velocity 
    rightVelocity = forward + angular * (wheelDistance/2) # calculate the right wheel velocity
    return [leftVelocity,rightVelocity]





# Trajectory planning: given target (ralative to robot frame), determine next forward/angular motion 
# Implement in a linear way
# If far away and facing wrong direction: rotate to face target
# If far away and facing target: move forward
# If on target: turOther Error Modelsn to desired orientation
def target_pose_to_velocity_linear(relativeTarget: Frame2D):
    # TODO
    
    velocity=0
    angular=0
    #target postion of x and y
    xTarget = relativeTarget.mat[0,2]
    yTarget = relativeTarget.mat[1,2]
    
    #checking the angle of the robot if it is actually positioned atan(y/x)
    
    angle = math.atan2(yTarget, xTarget)

    #print(yTarget, ' ', math.degrees(angle))


# If far away and facing wrong direction: rotate to face target
    if (abs(xTarget) > 8 or abs(yTarget) > 8):#offset from the target, this is the distance 8 cm away from the object
        if(abs(angle) > math.radians(12)):#not pointing to the target
            angular = (abs(angle) / angle) * (math.pi / 10)#18 degrees speed
            print("Rotate")          
            if(abs(angle) < math.radians(24)):#24 dgrees into radius, the angle should be less than 24 degrees and math.radians degrees into radian
                angular = (abs(angle) / angle) * (math.pi / 100) #1.8 degrees
                #print(angular)
                print("Rotating slower")
        elif(abs(angle) <= math.radians(12)):#when the angle is less than 12 degrees the velocity should be 12. 
            velocity = 15
            print("facing the target")
            
            

 # If on target: turn to desired orientation
    elif(abs(relativeTarget.angle()) > math.radians(12)):#not pointing to the target
        angular = (abs(relativeTarget.angle()) / relativeTarget.angle()) * (math.pi / 10)#18 degrees
        print("Rotate")          
        if(abs(relativeTarget.angle()) < math.radians(24)):
            angular = (abs(relativeTarget.angle()) / relativeTarget.angle()) * (math.pi / 100) #1.8 degrees
            #print(angular)
            print("Rotating slower")

    #if target is given after the angle is provided,
    #move it to the coordinates provided.

    #print(abs(yTarget) > 0.001)
   
    return [velocity, angular]


# Trajectory planning: given target (ralative to robot frame), determine next forward/angular motion 
# Implement by means of cubic spline interpolation 
def target_pose_to_velocity_spline(relativeTarget: Frame2D):
    # TODO

    velocity = 0
    angular = 0
    xTarget = relativeTarget.mat[0,2]
    yTarget = relativeTarget.mat[1,2]
   
    
    if(abs(xTarget) > 7 or abs(yTarget) > 7):
         s = math.sqrt(xTarget*xTarget+yTarget*yTarget)
         l = 20    # speed
         t = relativeTarget.mat[1,2]      # target coordinates
         hy = relativeTarget.mat[1,0] # y heading when moving from initial position

         k = 2 * (3 * t - s * hy) / (s * s)    # formula spline
  
         angular = l * k                       # length of the path, multiplcation with the spline
         velocity = l  
    
        
    return [velocity, angular]

def distance(measureDistance): # get the distance 
    ans = 1.0
    
    if(dist > 100 and dist < 400):
        ans = 0.5
    elif(dist >= 400 and dist < 510):
        ans = (0.5 * (dist - 400) / 110) + 0.5
    elif(dist > 50 and dist <= 100):
        ans = (1.0 - ((dist - 50) / 50)) * 0.5 + 0.5

    return ans


def cube_sensor_model(trueCubePosition, visible, measuredPosition):
    #print(measuredPosition.angle())
    pointX = trueCubePosition.mat[0, 2]
    pointY = trueCubePosition.mat[1, 2]
    
    dist = math.sqrt((pointX**2) + (pointY**2))# calculate the distance 
    if(visible):
        measure = measuredPosition.mult(trueCubePosition.inverse())#inverse the matrix
        pott = math.exp(-0.5 * ((m.x()/50)**2 + (m.y()/50)**2 + (m.angle()/50)**2))
         
        ans = pott
    else:
        ans = distance(measureDistance)
    return ans














